# HTML1 tutorial

tutorial Introduzione a HTML

HTML (HyperText Markup Language) è il linguaggio standard utilizzato per la realizzazione delle pagine Web e si basa sui TAG

i TAG sono i comandi dell'HTML, con i quali formattare la pagina, definire gli stili, caricare i contenuti e via dicendo

Una pagina WEB viene letta da un browser che interpreta i vari TAG.

I TAG vanno a coppie, 


uno di apertura <TAG>

uno di chiusura </TAG>

Ad esempio il tag body contiene l'intero contenuto della pagina web che sarà visualizzata da browser:

`<body>
	Questa è la pagina degli appunti
	del mio corso di coding
</body>`

In generale un pagina WEB è un documento HTML strutturato come segue:
`<html>

	<head>
	qui dentro ci sono cose che
	non vengono mostrate nella 
	pagina
	</head>

	<body>
	qui dentro c'è il contenuto
	</body>

</html>`
​
​
Tra i tag <html> </html> è racchiuso l'intero documento

Sulla destra in alto ci sono tre file: seleziona il file **index.html**

